# Robofriends

[https://www.vkainth.gitlab.io/robofriends](https://www.vkainth.gitlab.io/robofriends)

## Introduction

This is a simple Introduction to React project that goes over all the basic
concepts a React developer would need including
[Components and Props](https://reactjs.org/docs/components-and-props.html)
and [State and Lifecycle](https://reactjs.org/docs/state-and-lifecycle.html)

It is intended to be learnt from and developed upon.

It uses the very handy JSON Placeholder API to users and the Robohash API to
render the Robot cards.

## Technologies Used

- HTML and CSS

- React and JSX

- Javascript

- [Tachyons](http://tachyons.io/)

## Building

- Do `git clone https://gitlab.com/vkainth/robofriends`

- Run `cd robofriends`

- Run `yarn install` or `npm install`

- Run `yarn start` or `npm run start` to start the development server

- To build a production build, run `yarn build` or `npm run build`

- To view the production build, run `./node_modules/serve/bin/serve.js -s build`

## Attributions

- [Andrei Neagoie](https://github.com/aneagoie) for his articles and course

- [JSON Placeholder API](https://jsonplaceholder.typicode.com/)

- [Robohash API](http://robohash.org/)
