import React from 'react';
import {Card} from '../components';

const CardList = ({robots}) => {
	const cards = robots.map(user => {
		const {id, email, name} = user;
		return <Card key={id.toString()} email={email} id={id} name={name}/>;
	});
	return cards;
};

export {CardList};
