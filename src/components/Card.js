import React from 'react';
import PropTypes from 'prop-types';

const Card = ({email, id, name}) => (
	<div className="tc bg-light-green dib br3 pa3 ma2 grow bw2 shadow-5">
		<img src={`https://robohash.org/${id}?&size=200x200`} alt="robot"/>
		<div>
			<h2 className="f4">{name}</h2>
			<p className="lh-copy measure center f6 black-70">{email}</p>
		</div>
	</div>
);
Card.propTypes = {
	email: PropTypes.string,
	id: PropTypes.number,
	name: PropTypes.string
};

Card.defaultProps = {
	email: 'mail@mail.com',
	id: 1,
	name: 'Stranger'
};
export {Card};
