import React from 'react';
import PropTypes from 'prop-types';

const Scroll = ({children}) => {
	return (
		<div
			style={{overflowY: 'scroll', border: '1px solid black', height: '500px'}}
		>
			{children}
		</div>
	);
};

Scroll.propTypes = {
	children: PropTypes.object
};

Scroll.defaultProps = {
	children: {}
};
export {Scroll};
