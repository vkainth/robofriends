import React, {Component} from 'react';
import {CardList, SearchBox, Scroll} from '../components';
import './App.css';

class App extends Component {
	constructor() {
		super();
		this.state = {
			robots: [],
			searchfield: ''
		};
	}

	componentDidMount() {
		fetch('https://jsonplaceholder.typicode.com/users')
			.then(response => response.json())
			.then(users => {
				this.setState({robots: users});
			});
	}

	onSearchChange = event => {
		this.setState({searchfield: event.target.value});
	};

	render() {
		const filteredRobots = this.state.robots.filter(robot => {
			return robot.name
				.toLowerCase()
				.includes(this.state.searchfield.toLowerCase());
		});
		if (this.state.robots.length === 0) {
			return (
				<div className="tc">
					<h1>Loading...</h1>
				</div>
			);
		}
		return (
			<div className="tc">
				<h1 className="f2">Robot Friends</h1>
				<SearchBox searchChange={this.onSearchChange}/>
				<Scroll>
					<CardList robots={filteredRobots}/>
				</Scroll>
			</div>
		);
	}
}

export {App};
